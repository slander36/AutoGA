/*
 *	Computational Intelligence Shared Functions
 *	Created by Sean Lander
 *	Copyright (c) Optimatum - Optimize or Else.
 */

#ifndef _SHARED_FUNCTIONS_LIB_
#define _SHARED_FUNCTIONS_LIB_

// Library Includes
#include <cmath>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <map>
#include <bitset>
#include <vector>

// AGA Libraries
#include "Organism.cpp"

namespace cishared {
	float generateAlpha( int type, int t, int max ) {
		float alpha = 0.0f;
		float x = (float)t;
		float xmax = (float)max;

		switch(type) {
		// Case 0 - Quadratic
		case 0:
			alpha = exp( (5 / xmax) * ( x - xmax ) );
			break;
		// Case 1 - Exponential
		case 1:
			alpha = 1 - exp( -5 * x / xmax );
			break;
		// Case 2 and Default - Linear
		case 2:
		default:
			alpha = 1.0f * t / max;
		}
		return alpha;
	}
	
	// Create A Solution
	template <typename genetype>
	int createOrganism( Organism<genetype> ** callback,
		Organism<genetype> * lb,
		Organism<genetype> * ub ) {
		
		// Initialize Variables
		std::vector<genetype> lbv = lb->getDNA();
		std::vector<genetype> ubv = ub->getDNA();
		if( lbv.size() != ubv.size() ) return -1;
		int size = lbv.size();
		int diff = 0;
		std::vector<genetype> newv;

		// Create New Vector
		for( int i = 0 ; i < size ; i++ ) {
			diff = ubv[i] - lbv[i];
			newv.push_back( ( rand() % diff ) - lbv[i] );
		}

		*callback = new Organism<genetype>(newv);

		return 0;
	}

	// Mutate A Solution
	template <typename genetype>
	int mutateOrganism( Organism<genetype> * original, Organism<genetype> ** mutation ) {
		// Initialize Variables
		std::vector<genetype> dna = original->getDNA();
		std::vector<genetype> newdna;
		typename std::vector<genetype>::iterator it;
		for( it = dna.begin() ; it < dna.end() ; ++it ) {
			newdna.push_back( *it );
		}
		int size = newdna.size();
		int index = rand() % size;
		//newdna[index] = newdna[index] & newdna[(index + 1) % size];
		newdna[index] = rand() % size;

		// Create a Mutated Organism
		*mutation = new Organism<genetype>(newdna);
		if( *mutation == NULL )	return -1;
		return 0;
	}

	template <typename genetype>
	int generateFVariance( std::vector<Organism<genetype>*> population ) {
		int i, j, size = population.size();
		std::vector<double> variance(size, 0);
		float distance, fvariance;
		
		// Tally sum of fitness variance 
		for( i = 0 ; i < size - 1 ; ++i ) {
			for( j = i + 1 ; j < size ; ++j ) {
				distance = population[i]->distanceFrom(*population[j]);
				fvariance = (population[i]->getFitness() - population[j]->getFitness())/distance;
				variance[i] += fvariance;
				variance[j] += fvariance;
			}
		}
		
		// Update value of original
		for ( i = 0 ; i < size ; ++i ) { population[i]->setVarianceOfFitness(variance[i]); }
		
		return 0;
	}

// If Your Problem Has A Known MAXFITNESS
// Definte It In Problem Specific File
// With The Fitness Function
#ifndef MAXFITNESS
#define MAXFITNESS -1	
#endif

	template <typename genetype>
	bool maxfitness( Organism<genetype> * o ) {
		return ( o->getFitness() == MAXFITNESS );
	}

	template <typename genetype>
	void print_solution( Organism<genetype> * best ) {
		std::vector<genetype> bestv = best->getDNA();
		typename std::vector<genetype>::iterator it;
		std::cout << "The Solution Is: [";
		for( it = bestv.begin() ; it < bestv.end() ; it++ ) {
			std::cout << " " << *it;
		}
		std::cout << " ]" << std::endl;
		std::cout << "Fitness of Solution: " << best->getFitness() << std::endl;
		std::cout << "Fitness Diff: " << MAXFITNESS - best->getFitness() << std::endl;
		std::cout << "-----" << std::endl;
	}

	template <typename genetype>
	void print_solution_csv( int t, Organism<genetype> * best ) {
		std::cout << t << "," <<  MAXFITNESS - best->getFitness() << std::endl;
	}
}

#endif
