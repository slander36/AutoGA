/*
 *	Genetic Algorithm
 *	Created by Sean Lander
 *	Copyright (c) 2012 Optimatum - Optimize or Else.
 */

#ifndef _GENETICALGORITHM_LIB_
#define _GENETICALGORITHM_LIB_

// CI Dependencies
#include "Organism.cpp"
#include "QueenFunctions.cpp"
#include "SharedFunctions.cpp"

/*
 *	Genetic Algorithm
 */
namespace ga {
	template <typename genetype>
	int GA ( int(*fitness)(Organism<genetype>*),
		int(*generate)(Organism<genetype>**,
			Organism<genetype>*,
			Organism<genetype>*),
		int(*mutate)(Organism<genetype>*,
			Organism<genetype>**),
		Organism<genetype>** best,
		Organism<genetype> * lowerbound,
		Organism<genetype> * upperbound)
	{
		// Seed Random Number Generator
		srand( time(NULL) );

		// Check Inputs
		if ( lowerbound->getDNA().size() != upperbound->getDNA().size() )
			return NULL;

		// Initialize Variables
		//std::cout << "Num Queens: " << NUMGENES << std::endl;
		//std::cout << "Max Fitness: " << MAXFITNESS << std::endl;
		//std::cout << "-----" << std::endl;

		// Create Initial Population
		//std::cout << "Creating Initial Population" << std::endl;
		std::vector<Organism<genetype>*> population;

		int popsize = 32;

		Organism<genetype> * newo;
		for( int i = 0 ; i < popsize ; i++ ) {
			generate( &newo, lowerbound, upperbound );
			if( !newo ) {
				std::cout << "Creating Organism " << i << " failed." << std::endl;
			}
			fitness( newo );
			population.push_back( newo );
		}
		//std::cout << "Population Created" << std::endl;
		//std::cout << "-----" << std::endl;
		
		// Loop Variables
		Organism<genetype> * o1, * o2, * no1, * no2;
		int t = 0, T = 50000;
		typename std::vector<Organism<genetype>*>::iterator it;
		bool breakout = false;

		// Start Loop
		for( t = 0 ; t < T ; t++ ) {
			// Print Every 1000 Loops
			/*
			if ( t % 1000 == 0 ) {
				std::cout << ".";
				std::cout.flush();
			}
			*/

			// Calculate Fmax
			int fmax = 0;
			for( it = population.begin() ; it < population.end() ; ++it ) {
				fmax = ( (*it)->getFitness() > fmax ? (*it)->getFitness() : fmax );
			}

			// Create New Generation From XOver
			std::vector<Organism<genetype>*> newpopulation;
			while( newpopulation.size() < popsize ) {
				// Null Out Potential Mates
				o1 = NULL, o2 = NULL;
				
				// Pick Mate 1 - Elitist Selection
				float elitism;
				while( o1 == NULL ) {
					o1 = population[rand() % popsize];
					elitism = 1.0f / ( 1 + fmax - o1->getFitness() );
					if( rand() % 101 < floor(100 * elitism) ) break;
					o1 = NULL;
				}
				
				// Pick Mate 2 - Elitist Selection
				while( o2 == NULL ) {
					o2 = population[rand() % popsize];
					elitism = 1.0f / ( 1 + fmax - o2->getFitness() );
					if( rand() % 101 < floor(100 * elitism) ) break;
					o2 = NULL;
				}

				// Crossover
				std::bitset<NUMGENES> bitmask ( rand() );
				std::vector<genetype> oldchrom1, oldchrom2;
				oldchrom1 = o1->getDNA();
				oldchrom2 = o2->getDNA();
				std::vector<genetype> newchrom1, newchrom2;
				for( int i = 0 ; i < NUMGENES ; ++i ) {
					if( bitmask[i] ) {
						newchrom1.push_back( oldchrom1[i] );
						newchrom2.push_back( oldchrom2[i] );
					} else {
						newchrom1.push_back( oldchrom2[i] );
						newchrom2.push_back( oldchrom1[i] );
					}
				}
				no1 = new Organism<genetype>( newchrom1 );
				fitness( no1 );
				no2 = new Organism<genetype>( newchrom2 );
				fitness( no2 );
				newpopulation.push_back( no1 );
				newpopulation.push_back( no2 );
			}

			// Check If Best Possible In Pop
			for( it = newpopulation.begin() ; it < newpopulation.end() ; ++it ) {
				// If Maximum Fitness, Return Result
				if( cishared::maxfitness( *it ) ) {
					*best = *it;
					breakout = true;
					break;
				}
				// Else 10% Change to Mutate
				if( rand() % 101 < 10 ) {
					Organism<genetype> * neworganism;
					if( mutate( *it, &neworganism ) ) {
						std::cout << "Error Mutating Organism" << std::endl;
						return -1;
					}
					fitness( neworganism );
					// Check Again After Mutation
					if( cishared::maxfitness( neworganism ) ) {
						*best = neworganism;
						breakout = true;
					}
					// Swap old for new
					*it = neworganism;
				}
				if( breakout ) break;
			}

			//for( it = population.begin() ; it < population.end() ; ++it ) {
			//	delete (*it);
			//}
			population.clear();

			// Set Current Population To One Created Above
			population = newpopulation;

			if( breakout )  break;
		}
		//std::cout << std::endl;
		//std::cout << "Stopped At Iteration " << t << std::endl;
		//std::cout << "-----" << std::endl;
		// End Loop

		// Return Best Organism
		if( !breakout ) {
			*best = *(population.begin());
			fitness( *best );
			for( it = population.begin() ; it < population.end() ; ++it ) {
				fitness( *it );
				if( (*best)->lessFit( **it ) ) *best = *it;
			}
		}

		//cishared::print_solution<genetype>( *best );
		cishared::print_solution_csv<genetype>( t, *best );

		return 0;
	}
}

/*
 *	Main Funtion
 *	Remove After Testing
 */

/* 
int main( int argc, char** argv ) {
	// Initialize Variables
	std::vector<int> lbv;
	std::vector<int> ubv;
	for( int i = 0 ; i < NUMGENES ; i++ ) {
		lbv.push_back(0);
		ubv.push_back(NUMGENES - 1);
	}
	Organism<int> * lb = new Organism<int>(lbv);
	Organism<int> * ub = new Organism<int>(ubv);
	Organism<int> * best;

	std::cout << "*****" << std::endl;
	std::cout << "Starting Genetic Algorithm" << std::endl;
	if( GA<int>( queen::queens_fitness,
			queen::createOrganism,
			queen::mutateOrganism,
			&best,
			lb,
			ub ) ) {
		std::cout << "Error Running Genetic Algorithm" << std::endl;
		return -1;
	}
	std::cout << "Genetic Algorithm Finished" << std::endl;
	std::cout << "*****" << std::endl;

	return 0;
}
*/

#endif
