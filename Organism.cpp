/* Created and copyrighted by Zachary J. Fields. All rights reserved. */

#ifndef __ORGANISM_CPP__

#define __ORGANISM_CPP__

#include <cfloat>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <vector>

template <typename genotype>
class Organism {
  public:
	// Declaration(s)
	typedef std::vector<genotype> chromosome;
	
	// Constructor and destructor method(s)
	Organism (chromosome &_dna)
	:
		__dna(_dna),
		__fitness(-DBL_MAX),
		__variance_exploration(0),
		__variance_fitness(0)
	{}
	
	~Organism (void) { __dna.clear(); }

	// Accessor method(s)
	const chromosome & getDNA (void) const { return __dna; }
	
	const double getFitness (void) const { return __fitness; }
	
	const double getVarianceOfExploration (void) const { return __variance_exploration; }
	
	const double getVarianceOfFitness (void) const { return __variance_fitness; }
	
	void setDNA (chromosome &_dna) { __dna = _dna; }
	
	void setFitness (double _fitness) { __fitness = _fitness; }
	
	void setVarianceOfFitness (double _vof) { __variance_fitness = _vof; }
	
	// Public instance variable(s)
	// Public method(s)
	const double distanceFrom (const Organism<genotype> &_other) const {
		typename chromosome::const_iterator my_it, other_it, my_end = __dna.end();
		double l2_norm_sq = 0;
		
		for ( my_it = __dna.begin(), other_it = _other.getDNA().begin() ; my_it != my_end ; ++my_it, ++other_it ) {
			l2_norm_sq += (((*my_it) - (*other_it)) * ((*my_it) - (*other_it)));
		}
		
		return sqrt(l2_norm_sq);
	}
	
	const bool lessFit (const Organism<genotype> &_other) const { return ( getFitness() < _other.getFitness() ); }
	
	const bool lessVarianceOfExploration (const Organism<genotype> &_other) const { return ( getVarianceOfExploration() < _other.getVarianceOfExploration() ); }
	
	const bool lessVarianceOfFitness (const Organism<genotype> &_other) const { return ( getVarianceOfFitness() < _other.getVarianceOfFitness() ); }
	
	const double rankVarianceOfExploration ( std::vector<std::pair<double, double> > &_population_statistics, int _population_size ) {
		if ( _population_statistics.size() != __dna.size() ) return -1;
		__variance_exploration = 0;
		typename chromosome::const_iterator d_it, d_end = __dna.end();
		typename std::vector<std::pair<double, double> >::const_iterator s_it = _population_statistics.begin();
		
		for ( d_it = __dna.begin() ; d_it != d_end ; ++d_it, ++s_it ) {
			__variance_exploration += (((*d_it) - (*s_it).first) / (*s_it).second / std::sqrt(_population_size));
		}
		
		return __variance_exploration;
	}

  protected:
	// Protected instance variable(s)
	// Protected method(s)
	unsigned long generateBitMask (void) { return rand(); }
	
	unsigned long generateCrossoverPoint (void) {
		if ( __dna.empty() ) return 0;
		unsigned long cp(0);
		
		while ( !cp ) { cp = (rand() % __dna.size()); }
		
		return cp;
	}
	
	void setVarianceOfExploration (double _voe) { __variance_exploration = _voe; }
	
  private:
	// Private instance variable(s)
	chromosome __dna;
	double __fitness;
	double __variance_exploration;
	double __variance_fitness;
	
	// Private method(s)
};

#endif

/* Created and copyrighted by Zachary J. Fields. All rights reserved. */
