#include <iostream>
#include <stdlib.h>

#include "Matrix.h"
#include "Chromosome.h"

typedef struct {
	Matrix * chromosome;
	float fitness;
	float variance;
} Individual;

typedef struct {
	int size;
	Individual * pop;
	Individual * HoF;
	Matrix * ub;
	Matrix * lb;
} Population;

void Compute( Population *pop, int type, void(*func)(Individual*) );
