/* Created and copyrighted by Zachary J. Fields. All rights reserved. */

#ifndef __AUTOGA_CPP__
#define __AUTOGA_CPP__

// CI Dependencies
#include "Organism.cpp"
#include "QueenFunctions.cpp"
#include "SharedFunctions.cpp"

namespace autoga {
	typedef std::vector<std::pair<double, double> > variance_statistics;
	template <typename genetype>
	variance_statistics calcPopulationVarianceStatistics (std::vector<Organism<genetype>*> &_population) {
		variance_statistics population_statistics;
		if ( _population.empty() ) return population_statistics;
		
		typename std::vector<Organism<genetype>*>::const_iterator it, end = _population.end();
		int n = _population.front()->getDNA().size();
		double sum[n], sq_sum[n];
		for( int i = 0 ; i < n ; i++ ) sum[i] = sq_sum[i] = 0;
		
		typename std::vector<genetype> chromosome;
		for ( it = _population.begin() ; it != end ; ++it ) {
			chromosome = (*it)->getDNA();
			for ( int i = 0 ; i < n ; ++i ) {
				sum[i] += chromosome[i];
				sq_sum[i] += chromosome[i] * chromosome[i];
			}
		}
		for ( int i = (n - 1) ; i <= 0 ; --i ) {
			population_statistics.push_back(std::make_pair((sum[i] / n), ((n*sq_sum[i] - sum[i]*sum[i])/(n*(n-1)))));
		}
		
		return population_statistics;
	}

	/*
	 *	Automated Genetic Algorithm
	 */
	template <typename genetype>
	int AGA ( int(*fitness)(Organism<genetype>*),
		int(*generate)(Organism<genetype>**,
			Organism<genetype>*,
			Organism<genetype>*),
		int(*mutate)(Organism<genetype>*,
			Organism<genetype>**),
		Organism<genetype>** best,
		Organism<genetype> * lowerbound,
		Organism<genetype> * upperbound)
	{
		// Seed Random Number Generator
		srand( time(NULL) );

		// Check Inputs
		if ( lowerbound->getDNA().size() != upperbound->getDNA().size() )
			return NULL;

		// Initialize Variables
		//std::cout << "Num Queens: " << NUMGENES << std::endl;
		//std::cout << "Max Fitness: " << MAXFITNESS << std::endl;
		//std::cout << "-----" << std::endl;

		// Create Initial Population
		//std::cout << "Creating Initial Population" << std::endl;
		std::vector<Organism<genetype>*> population;

		int popsize = 32;

		Organism<genetype> * newo;
		for( int i = 0 ; i < popsize ; i++ ) {
			generate( &newo, lowerbound, upperbound );
			if( !newo ) {
				std::cout << "Creating Organism " << i << " failed." << std::endl;
			}
			fitness( newo );
			population.push_back( newo );
		}
		//std::cout << "Population Created" << std::endl;
		//std::cout << "-----" << std::endl;
		
		// Loop Variables
		Organism<genetype> * o1, * o2, * no1, * no2;
		int t = 0, T = 50000;
		typename std::vector<Organism<genetype>*>::iterator it;
		bool breakout = false;
		float alpha, beta;

		// Start Loop
		for( t = 0 ; t < T ; t++ ) {
			// Print Every 1000 Loops
			/*
			if ( t % 1000 == 0 ) {
				std::cout << ".";
				std::cout.flush();
			}
			*/

			// Calculate alpha and beta
			alpha = cishared::generateAlpha(1, t, T);
			beta = cishared::generateAlpha(1, t, T);

			// Calculate fmax, evmax and fvmax
			double fmax, evmax, newevmax, fvmax;
			fmax = evmax = newevmax = fvmax = 0;

			variance_statistics vs = calcPopulationVarianceStatistics(population);
			
			if( cishared::generateFVariance<genetype>(population) ) {
				std::cout << "Failed to Generate FVariance" << std::endl;
			}
			
			for( it = population.begin() ; it < population.end() ; ++it ) {
				fmax = ( (*it)->getFitness() > fmax ?
					(*it)->getFitness() :
					fmax );
				
				newevmax = (*it)->rankVarianceOfExploration(vs, population.size());
				evmax = ( newevmax > evmax ? newevmax : evmax );
				
				fvmax = ( (*it)->getVarianceOfFitness() > fvmax ?
					(*it)->getVarianceOfFitness() :
					fvmax );
			}

			// Create New Generation From XOver
			std::vector<Organism<genetype>*> newpopulation;

			int evsize = ( 1 - alpha ) * ( 1 - beta ) * popsize;
			int fvsize = ( 1 - alpha ) * ( beta ) * popsize;
			int fisize = popsize - evsize - fvsize;
			int randindex = 0, killme = 0;
			float elitism;
			
			// Select Based On Exploration Variance
			for( int i = 0 ; i < evsize ; ++i ) {
				// Null Out Potential Mates
				o1 = NULL, o2 = NULL;
				
				// Pick Mate 1 - Elitist Selection
				randindex = rand() % popsize;
				killme = 0;
				while( o1 == NULL ) {
					//std::cout << "11";
					//std::cout.flush();
					o1 = population[randindex];
					elitism = 1.0f / ( 1 + evmax - o1->getVarianceOfExploration() );
					if( rand() % 101 < floor(100 * elitism) ) break;
					if( ++killme > popsize ) break;
					o1 = NULL;
					randindex = (randindex + 1) % popsize;
				}
				//std::cout << std::endl;
				
				// Pick Mate 2 - Elitist Selection
				randindex = rand() % popsize;
				killme = 0;
				while( o2 == NULL ) {
					//std::cout << "12";
					//std::cout.flush();
					o2 = population[randindex];
					elitism = 1.0f / ( 1 + evmax - o2->getVarianceOfExploration() );
					if( rand() % 101 < floor(100 * elitism) ) break;
					if( ++killme > popsize ) break;
					o2 = NULL;
					randindex = (randindex + 1) % popsize;
				}
				//std::cout << std::endl;

				// Crossover
				std::bitset<NUMGENES> bitmask ( rand() );
				std::vector<genetype> oldchrom1, oldchrom2;
				oldchrom1 = o1->getDNA();
				oldchrom2 = o2->getDNA();
				std::vector<genetype> newchrom1, newchrom2;
				for( int i = 0 ; i < NUMGENES ; ++i ) {
					if( bitmask[i] ) {
						newchrom1.push_back( oldchrom1[i] );
						newchrom2.push_back( oldchrom2[i] );
					} else {
						newchrom1.push_back( oldchrom2[i] );
						newchrom2.push_back( oldchrom1[i] );
					}
				}
				no1 = new Organism<genetype>( newchrom1 );
				fitness( no1 );
				no2 = new Organism<genetype>( newchrom2 );
				fitness( no2 );
				newpopulation.push_back( no1 );
				++i;
				if( i < evsize )
					newpopulation.push_back( no2 );
			}

			// Select Based On Fitness Variance
			for( int i = 0 ; i < fvsize ; ++i ) {
				// Null Out Potential Mates
				o1 = NULL, o2 = NULL;
				
				// Pick Mate 1 - Elitist Selection
				randindex = rand() % popsize;
				killme = 0;
				while( o1 == NULL ) {
					//std::cout << "21";
					//std::cout.flush();
					o1 = population[randindex];
					elitism = 1.0f / ( 1 + fvmax - o1->getVarianceOfFitness() );
					if( rand() % 101 < floor(100 * elitism) ) break;
					if( ++killme > popsize ) break;
					o1 = NULL;
					randindex = (randindex + 1) % popsize;
				}
				//std::cout << std::endl;
				
				// Pick Mate 2 - Elitist Selection
				randindex = rand() % popsize;
				killme = 0;
				while( o2 == NULL ) {
					//std::cout << "22";
					//std::cout.flush();
					o2 = population[randindex];
					elitism = 1.0f / ( 1 + fvmax - o2->getVarianceOfFitness() );
					if( rand() % 101 < floor(100 * elitism) ) break;
					if( ++killme > popsize ) break;
					o2 = NULL;
					randindex = (randindex + 1) % popsize;
				}
				//std::cout << std::endl;

				// Crossover
				std::bitset<NUMGENES> bitmask ( rand() );
				std::vector<genetype> oldchrom1, oldchrom2;
				oldchrom1 = o1->getDNA();
				oldchrom2 = o2->getDNA();
				std::vector<genetype> newchrom1, newchrom2;
				for( int i = 0 ; i < NUMGENES ; ++i ) {
					if( bitmask[i] ) {
						newchrom1.push_back( oldchrom1[i] );
						newchrom2.push_back( oldchrom2[i] );
					} else {
						newchrom1.push_back( oldchrom2[i] );
						newchrom2.push_back( oldchrom1[i] );
					}
				}
				no1 = new Organism<genetype>( newchrom1 );
				fitness( no1 );
				no2 = new Organism<genetype>( newchrom2 );
				fitness( no2 );
				newpopulation.push_back( no1 );
				++i;
				if( i < fvsize )
					newpopulation.push_back( no2 );
			}

			// Select Based On Fitness
			for( int i = 0 ; i < fisize ; ++i ) {
				// Null Out Potential Mates
				o1 = NULL, o2 = NULL;
				
				// Pick Mate 1 - Elitist Selection
				randindex = rand() % popsize;
				killme = 0;
				while( o1 == NULL ) {
					//std::cout << "31";
					//std::cout.flush();
					o1 = population[randindex];
					elitism = 1.0f / ( 1 + fmax - o1->getFitness() );
					if( rand() % 101 < floor(100 * elitism) ) break;
					if( ++killme > popsize ) break;
					o1 = NULL;
					randindex = (randindex + 1) % popsize;
				}
				//std::cout << std::endl;
				
				// Pick Mate 2 - Elitist Selection
				randindex = rand() % popsize;
				killme = 0;
				while( o2 == NULL ) {
					//std::cout << "32";
					//std::cout.flush();
					o2 = population[randindex];
					elitism = 1.0f / ( 1 + fmax - o2->getFitness() );
					if( rand() % 101 < floor(100 * elitism) ) break;
					if( ++killme > popsize ) break;
					o2 = NULL;
					randindex = (randindex + 1) % popsize;
				}
				//std::cout << std::endl;

				// Crossover
				std::bitset<NUMGENES> bitmask ( rand() );
				std::vector<genetype> oldchrom1, oldchrom2;
				oldchrom1 = o1->getDNA();
				oldchrom2 = o2->getDNA();
				std::vector<genetype> newchrom1, newchrom2;
				for( int i = 0 ; i < NUMGENES ; ++i ) {
					if( bitmask[i] ) {
						newchrom1.push_back( oldchrom1[i] );
						newchrom2.push_back( oldchrom2[i] );
					} else {
						newchrom1.push_back( oldchrom2[i] );
						newchrom2.push_back( oldchrom1[i] );
					}
				}
				no1 = new Organism<genetype>( newchrom1 );
				fitness( no1 );
				no2 = new Organism<genetype>( newchrom2 );
				fitness( no2 );
				newpopulation.push_back( no1 );
				++i;
				if( i < fisize )
					newpopulation.push_back( no2 );
			}

			// Check If Best Possible In Pop
			for( it = newpopulation.begin() ; it < newpopulation.end() ; ++it ) {
				// If Maximum Fitness, Return Result
				if( cishared::maxfitness( *it ) ) {
					*best = *it;
					breakout = true;
					break;
				}
				// Else 10% Change to Mutate
				if( rand() % 101 < 10 ) {
					Organism<genetype> * neworganism;
					if( mutate( *it, &neworganism ) ) {
						std::cout << "Error Mutating Organism" << std::endl;
						return -1;
					}
					fitness( neworganism );
					// Check Again After Mutation
					if( cishared::maxfitness( neworganism ) ) {
						*best = neworganism;
						breakout = true;
					}
					// Swap old for new
					*it = neworganism;
				}
				if( breakout ) break;
			}

			//for( it = population.begin() ; it < population.end() ; ++it ) {
			//	delete (*it);
			//}
			population.clear();

			// Set Current Population To One Created Above
			population = newpopulation;

			if( breakout )  break;
		}
		//std::cout << std::endl;
		//std::cout << "Stopped At Iteration " << t << std::endl;
		//std::cout << "-----" << std::endl;
		// End Loop

		// Return Best Organism
		if( !breakout ) {
			*best = *(population.begin());
			fitness( *best );
			for( it = population.begin() ; it < population.end() ; ++it ) {
				fitness( *it );
				if( (*best)->lessFit( **it ) ) *best = *it;
			}
		}

		//cishared::print_solution<genetype>( *best );
		cishared::print_solution_csv<genetype>( t, *best );

		return 0;
	}
}

#endif

/* Created and copyrighted by Zachary J. Fields. All rights reserved. */
