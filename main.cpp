
// CI Dependencies
#include "SimulatedAnnealing.cpp"
#include "GeneticAlgorithm.cpp"
#include "AGA.cpp"

int runSimulatedAnnealing();
int runGeneticAlgorithm();
int runAutoGA();

int main( int argc, char** argv ) {
	std::cout << "N-Queens Optimization Comparison for AutoGA" << std::endl;
	std::cout << "Board Size: " << NUMGENES << "x" << NUMGENES << std::endl;
	
	// Run 100 Simulated Annealing and Print Results
	std::cout << "Simulated Annealing" << std::endl;
	for( int i = 0 ; i < 100 ; ++i ) {
		runSimulatedAnnealing();
	}

	// Run 100 Genetic Algorithms and Print Results
	std::cout << "Genetic Algorithm" << std::endl;
	for( int i = 0 ; i < 100 ; ++i ) {
		runGeneticAlgorithm();
	}
	
	// Run 100 AutoGA and Print Results
	std::cout << "AutoGA" << std::endl;
	for( int i = 0 ; i < 100 ; ++i ) {
		runAutoGA();
	}

	return 0;
}

int runSimulatedAnnealing () {
	// Initialize Variables
	Organism<int>* best;

	// Run Simulated Annealing
	if( sa::SA( queen::queens_fitness, queen::mutateOrganism, &best ) != 0 ) {
		std::cout << "Error Running Simulated Annealing" << std::endl;
		return -1;
	}

	delete best;

	// Return
	return 0;
}

int runGeneticAlgorithm() {
	// Initialize Variables
	std::vector<int> lbv;
	std::vector<int> ubv;
	for( int i = 0 ; i < NUMGENES ; i++ ) {
		lbv.push_back(0);
		ubv.push_back(NUMGENES - 1);
	}
	Organism<int> * lb = new Organism<int>(lbv);
	Organism<int> * ub = new Organism<int>(ubv);
	Organism<int> * best;

	// Run Genetic Algorithm
	if( ga::GA<int>( queen::queens_fitness,
			queen::createOrganism,
			queen::mutateOrganism,
			&best,
			lb,
			ub ) ) {
		std::cout << "Error Running Genetic Algorithm" << std::endl;
		return -1;
	}

	delete lb;
	lbv.clear();
	delete ub;
	ubv.clear();
	delete best;

	// Return
	return 0;
}

int runAutoGA() {
	// Initialize Variables
	std::vector<int> lbv;
	std::vector<int> ubv;
	for( int i = 0 ; i < NUMGENES ; i++ ) {
		lbv.push_back(0);
		ubv.push_back(NUMGENES - 1);
	}
	Organism<int> * lb = new Organism<int>(lbv);
	Organism<int> * ub = new Organism<int>(ubv);
	Organism<int> * best;

	// Run Genetic Algorithm
	if( autoga::AGA<int>( queen::queens_fitness,
			queen::createOrganism,
			queen::mutateOrganism,
			&best,
			lb,
			ub ) ) {
		std::cout << "Error Running Genetic Algorithm" << std::endl;
		return -1;
	}

	delete lb;
	lbv.clear();
	delete ub;
	ubv.clear();
	delete best;

	// Return
	return 0;
}

