/*
 *	N-Queens Functions
 *	Created by Sean Lander
 *	Copyright (c) 2012 Optimatum - Optimize or Else.
 */

#ifndef _QUEEN_FUNCTIONS_LIB_
#define _QUEEN_FUNCTIONS_LIB_

// Library Includes
#include <cstdlib>
#include <vector>

#define NUMGENES 64
#define MAXFITNESS ( NUMGENES * ( NUMGENES - 1 ) ) / 2

namespace queen {
	// Calculate A Queen Fitness
	int queens_fitness ( Organism<int> * organism ) {
		// Initialize Variables
		std::vector<int> chromosome = organism->getDNA();
		int i, j, size = chromosome.size(), fitness = 0;

		// O(n^2) Collision Check
		for( i = 0 ; i < size - 1 ; i++ ) {
			for( j = i + 1 ; j < size ; j++ ) {
				// If Horizontal or Diagonal Collision, Skip
				if(	chromosome[i] == chromosome[j] ||
						chromosome[i] - ( j - i ) == chromosome[j] ||
						chromosome[i] + ( j - i ) == chromosome[j] ) {
					continue;
				} else {
					// Otherwise, Add Non-Attacking Pair
					fitness++;
				}
			}
		}
		organism->setFitness(fitness);
		return 0;
	}
	
	// Create A Queens Solution
	template <typename genetype>
	int createOrganism( Organism<genetype> ** callback,
		Organism<genetype> * lb,
		Organism<genetype> * ub ) {
		
		// Initialize Variables
		std::vector<genetype> lbv = lb->getDNA();
		std::vector<genetype> ubv = ub->getDNA();
		if( lbv.size() != ubv.size() ) return -1;
		int size = lbv.size();
		int diff = 0;
		std::vector<genetype> newv;

		// Create New Vector
		for( int i = 0 ; i < size ; i++ ) {
			diff = ubv[i] - lbv[i];
			newv.push_back( ( rand() % diff ) - lbv[i] );
		}

		*callback = new Organism<genetype>(newv);

		return 0;
	}

	// Mutate A Solution
	template <typename genetype>
	int mutateOrganism( Organism<genetype> * original, Organism<genetype> ** mutation ) {
		// Initialize Variables
		std::vector<genetype> dna = original->getDNA();
		std::vector<genetype> newdna;
		typename std::vector<genetype>::iterator it;
		for( it = dna.begin() ; it < dna.end() ; ++it ) {
			newdna.push_back( *it );
		}
		int size = newdna.size();
		int index = rand() % size;
		newdna[index] = rand() % size;

		// Create a Mutated Organism
		*mutation = new Organism<genetype>(newdna);
		if( *mutation == NULL )	return -1;
		return 0;
	}
}

#endif
