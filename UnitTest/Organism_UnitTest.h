/* Created and copyrighted by Zachary J. Fields. All rights reserved. */

//#include <cxxtest/ErrorPrinter.h>
#include <cxxtest/GlobalFixture.h>
//#include <cxxtest/Mock.h>
#include <cxxtest/TestSuite.h>

#include <utility>

#include "AGA.cpp"
#include "Organism.cpp"

template <typename genotype>
class Organism_TEST : public Organism<genotype> {
  public:
	Organism_TEST (std::vector<genotype> &_dna)
	: Organism<genotype>(_dna)
	{}

	// Promote protected functions for testing
	using Organism<genotype>::generateBitMask;
	using Organism<genotype>::generateCrossoverPoint;
	using Organism<genotype>::setFitness;
	using Organism<genotype>::setVarianceOfExploration;
	using Organism<genotype>::setVarianceOfFitness;
};

class Organism_TestSuite : public CxxTest::TestSuite {
  public:
	bool setUpWorld (void) { return false; }
	bool tearDownWorld (void) { return false; }
	
	void setUp (void) {}
	void tearDown (void) {}
	
	void test_WhenInitialized_ThenDNAIsStored (void) {
		Organism<bool>::chromosome dna;
		{
			dna.push_back(false); dna.push_back(true); dna.push_back(true); dna.push_back(false);
			dna.push_back(false); dna.push_back(true); dna.push_back(false); dna.push_back(true);
		}
		
		Organism<bool> lucy(dna);
		
		TS_ASSERT_EQUALS(dna, lucy.getDNA());
	}
	
	void test_WhenInitialized_ThenFitnessIsNegativeDoubleMax (void) {
		Organism<bool>::chromosome dna;
		{
			dna.push_back(false); dna.push_back(true); dna.push_back(true); dna.push_back(false);
			dna.push_back(false); dna.push_back(true); dna.push_back(false); dna.push_back(true);
		}
		
		Organism<bool> lucy(dna);
		
		TS_ASSERT_EQUALS(-DBL_MAX, lucy.getFitness());
	}
	
	void test_WhenInitialized_ThenVarianceOfExplorationIsZero (void) {
		Organism<bool>::chromosome dna;
		{
			dna.push_back(false); dna.push_back(true); dna.push_back(true); dna.push_back(false);
			dna.push_back(false); dna.push_back(true); dna.push_back(false); dna.push_back(true);
		}
		
		Organism<bool> lucy(dna);
		
		TS_ASSERT_EQUALS(0, lucy.getVarianceOfExploration());
	}
	
	void test_WhenInitialized_ThenVarianceOfFitnessIsZero (void) {
		Organism<bool>::chromosome dna;
		{
			dna.push_back(false); dna.push_back(true); dna.push_back(true); dna.push_back(false);
			dna.push_back(false); dna.push_back(true); dna.push_back(false); dna.push_back(true);
		}
		
		Organism<bool> lucy(dna);
		
		TS_ASSERT_EQUALS(0, lucy.getVarianceOfFitness());
	}
	
	void test_WhenBitMaskIsGenerated_ThenValueIsRandom (void) {
		Organism<bool>::chromosome dna;
		{
			dna.push_back(false); dna.push_back(true); dna.push_back(true); dna.push_back(false);
			dna.push_back(false); dna.push_back(true); dna.push_back(false); dna.push_back(true);
		}
		Organism_TEST<bool> lucy(dna);
		
		TS_ASSERT_DIFFERS(lucy.generateBitMask(), lucy.generateBitMask());
	}
	
	void test_WhenCrossoverPointIsSelected_ThenValueIsRandom (void) {
		Organism<bool>::chromosome dna;
		{
			dna.push_back(false); dna.push_back(true); dna.push_back(true); dna.push_back(false);
			dna.push_back(false); dna.push_back(true); dna.push_back(false); dna.push_back(true);
		}
		Organism_TEST<bool> lucy(dna);
		
		TS_ASSERT_DIFFERS(lucy.generateCrossoverPoint(), lucy.generateCrossoverPoint());
	}
	
	void test_WhenCrossoverPointIsSelected_ThenValueInSpecifiedRange (void) {
		Organism<bool>::chromosome dna;
		{
			dna.push_back(false); dna.push_back(true); dna.push_back(true); dna.push_back(false);
			dna.push_back(false); dna.push_back(true); dna.push_back(false); dna.push_back(true);
		}
		
		Organism_TEST<bool> lucy(dna);
		unsigned long cp = lucy.generateCrossoverPoint();
		
		TS_ASSERT( cp == 1 || cp == 2 || cp == 3 || cp == 4 || cp == 5 || cp == 6 || cp == 7 );
	}
	
	void test_WhenOrganismIsLessFitThanComparedOrganism_ThenTrue (void) {
		Organism<bool>::chromosome dna;
		{
			dna.push_back(false); dna.push_back(true); dna.push_back(true); dna.push_back(false);
			dna.push_back(false); dna.push_back(true); dna.push_back(false); dna.push_back(true);
		}
		
		Organism_TEST<bool> ardi(dna);
		ardi.setFitness(2);
		Organism_TEST<bool> lucy(dna);
		lucy.setFitness(1);
		
		TS_ASSERT(lucy.lessFit(ardi));
	}
	
	void test_WhenOrganismIsMoreFitThanComparedOrganism_ThenFalse (void) {
		Organism<bool>::chromosome dna;
		{
			dna.push_back(false); dna.push_back(true); dna.push_back(true); dna.push_back(false);
			dna.push_back(false); dna.push_back(true); dna.push_back(false); dna.push_back(true);
		}
		
		Organism_TEST<bool> ardi(dna);
		ardi.setFitness(1);
		Organism_TEST<bool> lucy(dna);
		lucy.setFitness(2);
		
		TS_ASSERT(!lucy.lessFit(ardi));
	}
	
	void test_WhenOrganismHasLessVarianceOfExplorationThanComparedOrganism_ThenTrue (void) {
		Organism<bool>::chromosome dna;
		{
			dna.push_back(false); dna.push_back(true); dna.push_back(true); dna.push_back(false);
			dna.push_back(false); dna.push_back(true); dna.push_back(false); dna.push_back(true);
		}
		
		Organism_TEST<bool> ardi(dna);
		ardi.setVarianceOfExploration(2);
		Organism_TEST<bool> lucy(dna);
		lucy.setVarianceOfExploration(1);
		
		TS_ASSERT(lucy.lessVarianceOfExploration(ardi));
	}
	
	void test_WhenOrganismHasMoreVarianceOfExplorationThanComparedOrganism_ThenFalse (void) {
		Organism<bool>::chromosome dna;
		{
			dna.push_back(false); dna.push_back(true); dna.push_back(true); dna.push_back(false);
			dna.push_back(false); dna.push_back(true); dna.push_back(false); dna.push_back(true);
		}
		
		Organism_TEST<bool> ardi(dna);
		ardi.setVarianceOfExploration(1);
		Organism_TEST<bool> lucy(dna);
		lucy.setVarianceOfExploration(2);
		
		TS_ASSERT(!lucy.lessVarianceOfExploration(ardi));
	}

	void test_WhenOrganismHasLessVarianceOfFitnessThanComparedOrganism_ThenTrue (void) {
		Organism<bool>::chromosome dna;
		{
			dna.push_back(false); dna.push_back(true); dna.push_back(true); dna.push_back(false);
			dna.push_back(false); dna.push_back(true); dna.push_back(false); dna.push_back(true);
		}
		
		Organism_TEST<bool> ardi(dna);
		ardi.setVarianceOfFitness(2);
		Organism_TEST<bool> lucy(dna);
		lucy.setVarianceOfFitness(1);
		
		TS_ASSERT(lucy.lessVarianceOfFitness(ardi));
	}
	
	void test_WhenOrganismHasMoreVarianceOfFitnessThanComparedOrganism_ThenFalse (void) {
		Organism<bool>::chromosome dna;
		{
			dna.push_back(false); dna.push_back(true); dna.push_back(true); dna.push_back(false);
			dna.push_back(false); dna.push_back(true); dna.push_back(false); dna.push_back(true);
		}
		
		Organism_TEST<bool> ardi(dna);
		ardi.setVarianceOfFitness(1);
		Organism_TEST<bool> lucy(dna);
		lucy.setVarianceOfFitness(2);
		
		TS_ASSERT(!lucy.lessVarianceOfFitness(ardi));
	}
	
	void test_WhenRankingVarianceOfExploration_ThenValueIsReturned (void) {
		Organism<int>::chromosome dna;
		{
			dna.push_back(1); dna.push_back(4); dna.push_back(6); dna.push_back(2);
			dna.push_back(2); dna.push_back(4); dna.push_back(5); dna.push_back(7);
		}
		
		Organism<int> lucy(dna);
		unsigned int population_size(30);
		autoga::variance_statistics population_statistics;
		{
			population_statistics.push_back(std::make_pair(3, 1.3));
			population_statistics.push_back(std::make_pair(5, 2.3));
			population_statistics.push_back(std::make_pair(1, 0.4));
			population_statistics.push_back(std::make_pair(4, 3.3));
			population_statistics.push_back(std::make_pair(2, 1.2));
			population_statistics.push_back(std::make_pair(3, 1.5));
			population_statistics.push_back(std::make_pair(7, 0.7));
			population_statistics.push_back(std::make_pair(4, 1.6));
		}
		
		TS_ASSERT_DIFFERS(0, lucy.rankVarianceOfExploration(population_statistics, population_size));
	}

	void test_WhenRankingVarianceOfExploration_ThenValueIsSet (void) {
		Organism<int>::chromosome dna;
		{
			dna.push_back(1); dna.push_back(4); dna.push_back(6); dna.push_back(2);
			dna.push_back(2); dna.push_back(4); dna.push_back(5); dna.push_back(7);
		}
		
		Organism<int> lucy(dna);
		unsigned int population_size(30);
		autoga::variance_statistics population_statistics;
		{
			population_statistics.push_back(std::make_pair(3, 1.3));
			population_statistics.push_back(std::make_pair(5, 2.3));
			population_statistics.push_back(std::make_pair(1, 0.4));
			population_statistics.push_back(std::make_pair(4, 3.3));
			population_statistics.push_back(std::make_pair(2, 1.2));
			population_statistics.push_back(std::make_pair(3, 1.5));
			population_statistics.push_back(std::make_pair(7, 0.7));
			population_statistics.push_back(std::make_pair(4, 1.6));
		}
		lucy.rankVarianceOfExploration(population_statistics, population_size);
		
		TS_ASSERT_DIFFERS(0, lucy.getVarianceOfExploration());
	}
	
	void test_WhenDistanceFromAnotherOrganismIsRequested_ThenL2NormIsReturned (void) {
		Organism<int>::chromosome ardi_dna;
		{
			ardi_dna.push_back(9); ardi_dna.push_back(3); ardi_dna.push_back(2); ardi_dna.push_back(9);
			ardi_dna.push_back(1); ardi_dna.push_back(5); ardi_dna.push_back(6); ardi_dna.push_back(5);
		}
		
		Organism<int>::chromosome lucy_dna;
		{
			lucy_dna.push_back(3); lucy_dna.push_back(6); lucy_dna.push_back(7); lucy_dna.push_back(3);
			lucy_dna.push_back(2); lucy_dna.push_back(5); lucy_dna.push_back(8); lucy_dna.push_back(1);
		}
		
		Organism<int> ardi(ardi_dna);
		Organism<int> lucy(lucy_dna);
		
		double l2_norm = sqrt(pow((9-3), 2) + pow((3-6), 2) + pow((2-7), 2) + pow((9-3), 2) + pow((1-2), 2) + pow((5-5), 2) + pow((6-8), 2) + pow((5-1), 2));
		TS_ASSERT_EQUALS(l2_norm, ardi.distanceFrom(lucy));
	}
};

/* Created and copyrighted by Zachary J. Fields. All rights reserved. */
