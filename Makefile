CC=g++
CFLAGS=-I.
OFLAGS= -O2
SA = SimulatedAnnealing.o
GA = GeneticAlgorithm.o
AGA = AGA.o
MAIN = main.o
EXES = sa ga autoga sadb gadb autogadb all

%.o: %.c ${DEPS}
	${CC} -c -o $@ $<  ${OFLAGS} ${CFLAGS}
sa: ${SA}
	${CC} -o $@ $^ ${OFLAGS} ${CFLAGS}

sadb: ${SA}
	${CC} -g3 -o $@ $^ ${OFLAGS} ${CFLAGS}

ga: ${GA}
	${CC} -o $@ $^ ${OFLAGS} ${CFLAGS}

gadb: ${GA}
	${CC} -g3 -o $@ $^ ${OFLAGS} ${CFLAGS}

autoga: ${AGA}
	${CC} -o $@ $^ ${OFLAGS} ${CFLAGS}

autogadb: ${AGA}
	${CC} -g3 -o $@ $^ ${OFLAGS} ${CFLAGS}

all: ${MAIN}
	${CC} -o $@ $^ ${OFLAGS} ${CFLAGS}

clean:
	rm ${EXES} *.o
