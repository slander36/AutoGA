//
//  main.cpp
//  AutoGA
//
//  Created by Sean Lander on 11/20/12.
//  Copyright (c) 2012 Optimatum - Optimize Or Else. All rights reserved.
//

#include "autoga_lib.h"

using namespace std;

void fitness( Individual * ind ) {
	printf("Computing Fitness for Individual at memory %f\n", (void *)ind);
	if( ind->chromosome == NULL ) {
		printf("What the hell...\n");
		return;
	}
	printf("Value of Chromosome 1: %d\n", ind->chromosome->getValue()[0]);
	ind->fitness = (float)ind->chromosome->getValue()[0];
	printf("Fitness is %f\n", ind->fitness);
}

int main( int argc, char ** argv ) {
	// Initialize Local Variables
	int popSize = 10;
	int chromSize = 4;

	printf("Starting AutoGA\n");

	// Initialize Population
	Population * pop = (Population *) malloc( sizeof(Population *) );
	if( !pop ) {
		printf("Population Malloc Fail\n");
		return 1;
	}
	// Set Size
	pop->size = popSize;
	// Create Individuals
	pop->pop = (Individual *) malloc( sizeof(Individual *) * popSize );
	if( !(pop->pop) ) {
		printf("Individuals Array Malloc Fail\n");
		return 1;
	}

	int i;
	int *chrom;

	printf("Allocating Population Chromosomes\n");
	// Initialize Individuals
	for( i = 0 ; i < popSize ; i++ ) {
		printf("Creating Individual %d\n", i);
		// Create Chromosomes
		chrom = (int *)malloc( sizeof(int *) * chromSize );
		chrom[0] = (i + 3) % 5;
		chrom[1] = (2*i + 7) % 5;
		chrom[2] = (4*i - 2) % 5;
		chrom[3] = (7*i + 4) % 5;
		pop->pop[i].chromosome = new Matrix(chrom, chromSize);
		// Set Initial Fitness
		pop->pop[i].fitness = 0;
		// Set Initial Variance
		pop->pop[i].variance = 0;
	}

	printf("Computing Fitness\n");

	Compute( pop, 1, fitness );
    Compute( pop, 2, NULL);
}
