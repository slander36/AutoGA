/*
 *	Simulated Annealing
 *	Created by Sean Lander
 *	Copyright (c) 2012 Optimatum - Optimize or Else.
 */

#ifndef _SIMULATEDANNEALING_LIB_
#define _SIMULATEDANNEALING_LIB_

// CI Dependencies
#include "Organism.cpp"
#include "QueenFunctions.cpp"
#include "SharedFunctions.cpp"

namespace sa {
	/*
	 *	Simulated Annealing
	 */
	template <typename genetype>
	int SA ( int(*fitness)(Organism<genetype>*),
		int(*mutate)(Organism<genetype>*,
			Organism<genetype>**),
		Organism<genetype> ** best )
	{	
		// Seed Random Number Generator
		srand( time(NULL) );

		// Initialize Variables
		//std::cout << "Num Queens: " << NUMGENES << std::endl;
		//std::cout << "Max Fitness: " << MAXFITNESS << std::endl;
		//std::cout <<  "-----" << std::endl;

		// Initialize First Organism
		//std::cout << "Creating Initial Solution" << std::endl;
		std::vector<genetype> init;
		for( int i = 0 ; i < NUMGENES ; i++ ) {
			init.push_back(rand() % NUMGENES);
		}
		Organism<genetype> *current = new Organism<genetype>(init);
		if( fitness(current) != 0 ) {
			std::cout << "Failed to Caluclate Fitness" << std::endl;
			return -1;
		}
		//std::cout << "Initial Solution Created" << std::endl;
		//std::cout << "-----" << std::endl;

		// Initialize New Organism
		Organism<genetype> *next = NULL;
		
		// Initialize Tempurature Controls
		int t = 0;
		int a = 0;
		int T = 50000;

		// Start Loop
		//std::cout << "Running Loop" << std::endl;
		for( t = 0 ; t < T ; t++ ) {
			/*
			if( t % 1000 == 0 ) {
				std::cout << ".";
				std::cout.flush();
			}
			*/
			// Check If Best Possible (if this is an option)
			if( cishared::maxfitness(current) ) break;
			// Generate Alpha
			a = (int) (cishared::generateAlpha( 0, t, T ) * 1000);
			// Mutate Organism
			if ( mutate( current, &next ) ) {
				std::cout << "Failed to Mutate" << std::endl;
				return -1;
			}
			if( fitness(next) != 0 ) {
				std::cout << "Failed to Calculate Fitness" << std::endl;
				return -1;
			}
			// Check New Fitness Against Old
			if( current->lessFit( *next ) ) {
				// Take New If Better
				delete current;
				current = next;
			} else {
				// Else Take New If rand() > Alpha
				if( rand() % 1000 > a ) {
					delete current;
					current = next;
				}
			}
		}
		//std::cout << std::endl;
		//std::cout << "Stopped at iteration: " << t << std::endl;
		//std::cout << "-----" << std::endl;
		// End Loop

		*best = current;

		//cishared::print_solution<genetype>( *best );
		cishared::print_solution_csv<genetype>( t, *best );
		
		return 0;
	}
}

/*
 *	Main Function
 */

/*
int main ( int argc, char** argv ) {
	Organism<int>* best;
	std::cout << "*****" << std::endl;
	std::cout << "Starting Simulated Annealing" << std::endl;
	if( SA( queen::queens_fitness, queen::mutateOrganism, &best ) != 0 ) {
		std::cout << "Error Running Simulated Annealing" << std::endl;
		return -1;
	}
	std::cout << "Simualted Annleaing Finished" << std::endl;
	std::cout << "*****" << std::endl;

	return 0;
}
*/

#endif
